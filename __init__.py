import csv
import random
from string import ascii_lowercase, ascii_uppercase, punctuation, digits

with open('csffiles2.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    field = ["name", "size", "type", "entity"]
    writer.writerow(field)
    for _ in range(5):
        name = ''.join(random.choice(ascii_lowercase + ascii_uppercase + punctuation + digits) for _ in range(random.randint(0, 5)))
        size = random.randint(0, 10)
        type_value = random.randint(0, 10)
        entity = random.randint(1, 110)
        writer.writerow([name, size, type_value, entity])
with open('csffiles.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    field = ["title","name", "size", "type", "entity"]
    writer.writerow(field)
    for _ in range(5):
        name = ''.join(random.choice(ascii_lowercase + ascii_uppercase + punctuation + digits) for _ in range(random.randint(0, 5)))
        title = ''.join(random.choice(ascii_lowercase + ascii_uppercase + punctuation + digits) for _ in range(random.randint(0, 30)))
        size = random.randint(0, 10)
        type_value = random.randint(0, 10)
        entity = random.randint(1, 110)
        writer.writerow([name, size, type_value, entity])

with open('csffiles.csv','r') as c1,open('csffiles2.csv') as c2:
    file1=c1.readlines()
    file2=c2.readlines()
with open('diff.csv','w') as outf:
    for line in file2:
        if line not in file1:
            outf.write(line)